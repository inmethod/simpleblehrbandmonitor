package inmethod.android.bt.example.BleHeartRateBand;

import android.Manifest;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Message;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Vector;

import ble.std.protocol.BTCommandsHandler.BTCommandsHandler;
import ble.std.protocol.HearRate.BTCommandsHRMeasurement;
import ble.std.protocol.HearRate.HeartRateValue;
import inmethod.android.bt.BTInfo;
import inmethod.android.bt.DeviceConnection;
import inmethod.android.bt.command.BTCommands;
import inmethod.android.bt.handler.CommandCallbackHandler;
import inmethod.android.bt.handler.ConnectionCallbackHandler;
import inmethod.android.bt.handler.DiscoveryServiceCallbackHandler;
import inmethod.android.bt.interfaces.IChatService;
import inmethod.android.bt.interfaces.IDiscoveryService;
import inmethod.android.bt.le.LeChatService;
import inmethod.android.bt.le.LeDiscoveryService;

public class MainActivity extends AppCompatActivity {

    //  Global Object
    private IDiscoveryService aIBlueToothDiscoveryService = null;
    private DeviceConnection aBlueToothDeviceConnection = null;
    private IChatService aIBlueToothChatService = null;
    private AppCompatActivity activity = null;
    private int iCurrentHR;
    public static final String HeartRateUUID = "00002a37-0000-1000-8000-00805f9b34fb";
    private int iDefaultAge = 25;
    private String sDefaultDeviceName = "PS";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activity = this;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("This app needs location access");
                builder.setMessage("Please grant location access");
                builder.setPositiveButton(android.R.string.ok, null);
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    public void onDismiss(DialogInterface dialog) {
                        requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
                    }

                    ;
                });
                builder.show();
            }
            ;
            LocationManager locManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
            if (!locManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                // show open gps message
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Info");
                builder.setMessage("Please enable Location service(android 6)");
                builder.setPositiveButton("OK", new
                        DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // jump to setting
                                Intent enableGPSIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(enableGPSIntent);
                            }
                        });
                builder.show();
            }
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        iCurrentHR = 0;
        // Use BLE discovery Service
        aIBlueToothDiscoveryService = LeDiscoveryService.getInstance();

        // Set context()
        aIBlueToothDiscoveryService.setContext(activity);

        ///  Set device name filter
        Vector<String> aBlueToothDeviceNameFilter = new Vector<String>();
        aBlueToothDeviceNameFilter.add(sDefaultDeviceName);
        aIBlueToothDiscoveryService.setBlueToothDeviceNameFilter(aBlueToothDeviceNameFilter);

        // Set CallBack handler
        aIBlueToothDiscoveryService.setCallBackHandler(new MyBlueToothDiscoveryServiceCallbackHandler());

        try {
            aIBlueToothDiscoveryService.startService();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onStop();
        if (aBlueToothDeviceConnection != null && aBlueToothDeviceConnection.isConnected()) {
            aBlueToothDeviceConnection.stop();
            aBlueToothDeviceConnection = null;
        }
        if (aIBlueToothDiscoveryService != null && aIBlueToothDiscoveryService.isRunning()) {
            aIBlueToothDiscoveryService.cancelDiscovery();
            aIBlueToothDiscoveryService.stopService();
            aIBlueToothDiscoveryService = null;
        }
        this.finish();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (aBlueToothDeviceConnection != null && aBlueToothDeviceConnection.isConnected()) {
            aBlueToothDeviceConnection.stop();
            aBlueToothDeviceConnection = null;
        }
        if (aIBlueToothDiscoveryService != null && aIBlueToothDiscoveryService.isRunning()) {
            aIBlueToothDiscoveryService.cancelDiscovery();
            aIBlueToothDiscoveryService.stopService();
            aIBlueToothDiscoveryService = null;
        }
    }

    public class MyBlueToothDiscoveryServiceCallbackHandler extends DiscoveryServiceCallbackHandler {
        @Override
        public void StartServiceStatus(boolean bStatus , int iCode) {
            if(bStatus && iCode==DiscoveryServiceCallbackHandler.START_SERVICE_SUCCESS){
                Log.d(TAG, "StartDiscoveryServiceSuccess!");
                if (aIBlueToothDiscoveryService.isRunning())  aIBlueToothDiscoveryService.doDiscovery();
            }else if(!bStatus && iCode==DiscoveryServiceCallbackHandler.START_SERVICE_BLUETOOTH_NOT_ENABLE){
                Toast.makeText(activity, "SERVICE_BLUETOOTH_NOT_ENABLE ", Toast.LENGTH_SHORT).show();
            }
        }
        @Override
        public void DeviceDiscoveryStatus(boolean bStatus, BTInfo aBTInfo) {
            if(bStatus){
                // cancel discovery
                aIBlueToothDiscoveryService.cancelDiscovery();

                // enable notification
                ArrayList<String> aNotifyUUIDs = new ArrayList<String>();
                aNotifyUUIDs.add(HeartRateUUID);

                aIBlueToothChatService = new LeChatService(BluetoothAdapter.getDefaultAdapter(), activity, aNotifyUUIDs);

                // create new connection object and set up call back handler
                aBlueToothDeviceConnection = new DeviceConnection(aBTInfo, activity,
                        aIBlueToothChatService, new MyBlueToothConnectionCallbackHandler());

                // Connect to device
                aBlueToothDeviceConnection.connect();

                Toast.makeText(activity, "device=" + aBTInfo.getDeviceName() + ",address=" + aBTInfo.getDeviceAddress(),
                        Toast.LENGTH_SHORT).show();

            }else if(!bStatus){
                Log.d(TAG, "device not found!");
                Toast.makeText(activity, "device not found!", Toast.LENGTH_SHORT).show();
                aIBlueToothDiscoveryService.doDiscovery();
            }
        }

    }

    public class MyBlueToothConnectionCallbackHandler extends ConnectionCallbackHandler {
        HeartRateValue aHeartRateValue = null;
        @Override
        public void DeviceConnectionStatus(boolean bStatus, BTInfo aBTInfo) {

            if(bStatus){
                Toast.makeText(activity, "Device Connected", Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(activity, "Connection lost!", Toast.LENGTH_SHORT).show();
                if (aBlueToothDeviceConnection != null && aBlueToothDeviceConnection.isConnected()) {
                    Log.d(TAG, "stop connection!");
                    aBlueToothDeviceConnection.stop();
                }
            }
        }

        @Override
        public void NotificationStatus(boolean bStatus,BTInfo aBTInfo, String sNotificationUUID) {
            if(bStatus){
                BTCommands aBTCommands = new BTCommandsHRMeasurement(iDefaultAge);
                aBTCommands.setCallBackHandler(
                        new BTCommandsHandler() {
                            @Override
                            public void BTCommandsHeartRateMeasurementSuccess(HeartRateValue aHeartRateValue,BTInfo aInfo){
                                if (aHeartRateValue == null) return;
                                else iCurrentHR = aHeartRateValue.getHeartRate();
                                Toast.makeText(activity, "Heart Rate = " + iCurrentHR, Toast.LENGTH_SHORT).show();
                                Log.d(TAG, "Current HeartRate=" + iCurrentHR);
                            }

                            @Override
                            public void BTCommandsHeartRateMeasurementTimeout(BTInfo aInfo){
                                Toast.makeText(activity, "command Timeout", Toast.LENGTH_SHORT).show();
                                Log.d(TAG, "command Timeout");
                                aBlueToothDeviceConnection.stop();
                            }
                        }
                );
                aBlueToothDeviceConnection.sendBTCommands(aBTCommands);

            }else if(!bStatus){
              Log.d(TAG, "NotificationEnableFail");
            }
      }


        @Override
        public void NotificationEnabled(BTInfo aBTInfo, String sReaderUUID) {

        }
    }
}