Note:
> Default Device Name Filter is "PS"    
     

# Simple BLE  Heart Rate Band Measurement  App
This App is example to active and read BLE standard Heart Rate Band measurement by using InMethod Android BlueTooth Framework     
https://bitbucket.org/inmethod/bluetoothframework    
and Bluetooth standard protocol
https://bitbucket.org/inmethod/blestdprocotol

## System Requirement 
* Android 4.3 or above
* Support Classic Bluetooth and Bluetooth Low Energy

## Develop Environment
* Android Studio 2.0 or above

## BlueTooth Heart Rate Measurement ( 0x2A37 )
https://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.heart_rate_measurement.xml    

    
